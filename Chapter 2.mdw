# Chapter 2: The Garden of Forking Data

<<>>=
ways = [0 3 8 9 0]
ways / sum(ways)  # some basic probabilities
@

## 2.2 Building a Model
Below is our english-language definition of the globe model

1. The true proportion of water covering the globe is $p$
2. A single toss of the globe has a probability $p$ of producing a water $W$
   observation. It has a probability $1 - p$ of producing a land ($L$)
   observation
3. Each toss of the globe is independent of the others

This can be represented using the binomial distribution:

\[ Pr(w|n, p) = \frac{n!}{w!(n - w)!} p^w (1 - p)^{n - w} \]

which specifieds the probability of observing $w$ $W$s in $n$ tosses, with a
probability $p$ of $W$.

<<>>=
using Stan, Gadfly, Mamba

set_cmdstan_home!("/usr/local/opt/cmdstan")


const earthmodel = "
data {
  int<lower=1> N;
  int<lower=0> k;
}
parameters {
  real<lower=0, upper=1> theta;
  real<lower=0, upper=1> thetaprior;

}
model {
  theta ~ beta(1, 1);
  thetaprior ~ beta(1, 1);
  k ~ binomial(N, theta);
}
generated quantities {
    int<lower=0> postpredk;
    int<lower=0> priorpredk;
    postpredk <- binomial_rng(N, theta);
    priorpredk <- binomial_rng(N, thetaprior);
  }
"
stanmodel = Stanmodel(name="earthbinomial", model=earthmodel);
earthdata = Dict("N" => 9,
                 "k" => 6)

rc, sim1 = stan(stanmodel, [earthdata], pwd(), CmdStanDir=ENV["CMDSTAN_HOME"])
if rc == 0
   theta_sim = sim1[1:size(sim1, 1), ["theta", "thetaprior"], :]
   predk_sim = sim1[1:size(sim1,1), ["postpredk", "priorpredk"], :]
   p1 = plot(theta_sim, [:trace, :mean, :density, :autocor], legend=false);
   draw(p1, ncol=4, nrow=2)
   p2 = plot(predk_sim, [:bar], legend=false)
   draw(p2, ncol=2, nrow=2)
end
describe(theta_sim)
@

## Fitting Models

1. Grid approximation
2. Quadratic approximation
3. Markov chain Monte Carlo (MCMC)

### Grid approximation

1. Defined the grid. How many points are used in estimating the posterior?
2. Compute the value of the prior at each parameter value
3. Compute the likelihood at each parameter value
4. Compute the unstandardized posterior at each value by multiplying the prior
   by the likelihood
5. Standardize the posterior.

<<>>=
# %%
using Gadfly, Distributions

function grid_approx(dpdf, ps, priorfn::Function)
   p_grid = linspace(0, 1, ps[:samples])  # Define the grid
   prior = priorfn(p_grid, ps)            # Define the prior
   likelihood = dpdf(p_grid, ps)          # Compute the likelihood @ each grid point
   unstd_posterior = likelihood .* prior  # Compute product of likelihood and prior
   posterior = unstd_posterior / sum(unstd_posterior) # Standardize that shit
   (p_grid, posterior)
end

function plot_approx(dpdf, params, priorfn)
   p_grid, posterior = grid_approx(dpdf, params, priorfn)
   plot(x=p_grid, y=posterior, Geom.line,
        Guide.xlabel("P(W)"),
        Guide.ylabel("posterior probability"))
end

uniform_prior = (grid, params) -> ones(params[:samples])
dbinom = ((grid, params) -> [pdf(Binomial(params[:n], p), params[:k]) for p ∈ grid])

# More samples -> higher fidelity (but higher processing costs!)
plot_approx(dbinom, Dict(:samples=>20, :n=>9, :k=>6), uniform_prior)


plot_approx(dbinom, Dict(:samples=>, :n=>9, :k=>6), uniform_prior)
plot_approx(dbinom, Dict(:samples=>100, :n=>9, :k=>6), uniform_prior)
plot_approx(dbinom, Dict(:samples=>20, :n=>9, :k=>6),
      (g, p) -> exp.( -5 .* abs.(g .- 0.5)))
@

I'm not going to implement MAP here because `JuMP` is asking for the derivative
of the factorial function and I don't believe that's defined...

## Exercises

<<>>=
# %%

@


















2E1 2 $Pr(rain|Monday)$

2M1
<<>>=
function mkparams(s::String; samples=20)
   k = count(c -> c == 'W', s)
   n = length(s)
   Dict(:samples=>samples, :k=>k, :n=>n)
end
p1 = plot_approx(dbinom, mkparams("WWW"), uniform_prior)
p2 = plot_approx(dbinom, mkparams("WWWL"), uniform_prior)
p3 = plot_approx(dbinom, mkparams("LWWLWWW", samples=20), uniform_prior)
p3
@

2M2
<<>>=
step_prior = (grid, ps) -> map(p -> ifelse(p < 0.5, 0, 12), grid)
plot_approx(dbinom, mkparams("WWW"), step_prior)
plot_approx(dbinom, mkparams("WWWL"), step_prior)
plot_approx(dbinom, mkparams("LWWLWWW", samples=100), step_prior)
@

2M3
# <codecell>
Pr(Earth) = Pr(Mars) = 0.5
Pr(land|Earth) = 0.30
Pr(land|Mars) = 1.00
Pr(land) = Pr(land|Earth) x Pr(Earth) + Pr(land|Mars) x Pr(Mars)
         = 0.30 x 0.5 + 1.0 x 0.5
         = .65

Pr(Earth|land) x Pr(land) = Pr(land|Earth) x Pr(Earth)
Pr(Earth|land) x 0.65 = 0.3 x 0.5
Pr(Earth|land) = Pr(land|Earth) x Pr(Earth) / Pr(land)
               = 0.3 * 0.5 / 0.65
               = .230769231

2M4
One card has two black sides (c1), one card has two white sides (c2),
one card has 1 black 1 white side (c3)

<<>>=
# %%
ways_c1 = 2
ways_c2 = 0
ways_c3 = 1
p_c1 = 2 / 3
p_c2 = 0
p_c3 = 1 / 3
@

The card being c2 is the _only_ way to have a black back, so 2/3

2M5
<<>>=
# %%
ways_c1 = 2
ways_c2 = 0
ways_c3 = 1
ways_c4 = 2
tot = ways_c1 + ways_c2 + ways_c3 + ways_c4
p_c1 = ways_c1 / tot
p_c2 = ways_c2 / tot
p_c3 = ways_c3 / tot
p_c4 = ways_c4 / tot
pr_back_black = p_c1 + p_c2
@

2M6
<<>>=
# %%
function p2m6()
   ways = [2 1 1 0 0 0]
   probs = ways ./ sum(ways)
   probs[1]
end
@assert p2m6() == 0.5
@

2M7
<<>>=
# %%
function p2m7()
   ways_fst_black = [2 1 0]
   p_fc = ways_fst_black ./ sum(ways_fst_black)
   ways_snd_white = [0 1 2]
   p_sc = ways_snd_white ./ sum(ways_snd_white)
   ways_fst_2b = ways_fst_black[1] * sum(ways_snd_white)
   ways_fst_2b / (ways_fst_2b + ways_fst_black[2] * ways_snd_white[3])
end

@assert p2m7() == 0.75
@

2H1
<<>>=
# %%
pA = 0.5
pB = 0.5
ptwins_given_A = 0.10
ptwins_given_B = 0.20
ptwins = ptwins_given_A * pA + ptwins_given_B * pB
pA_given_twins = ptwins_given_A * pA / ptwins
pB_given_twins = ptwins_given_B * pB / ptwins
ptwins_given_twins = ptwins_given_A * pA_given_twins +
                     ptwins_given_B * pB_given_twins
@assert ptwins_given_twins == 1/6

# %%
pA_given_twins = ptwins_given_A * pA / ptwins
@assert pA_given_twins == 1/3


psingle_g_A = 1 - ptwins_given_A
ptwins_single = ptwins_given_A * (1-ptwins_given_A) * pA +
                  ptwins_given_B * (1-ptwins_given_B) * pB
pA_g_twins_single = ptwins_given_A * (1-ptwins_given_A) * pA / ptwins_single
@assert pA_g_twins_single == 0.360

# As the twins and single are independent we use the joint distribution!
# p2h4
p_ta_g_a = 0.8
p_tb_g_b = 0.65
p_ta_g_b = 1 - p_tb_g_b
p_ta = p_ta_g_a  * pA + p_ta_g_b * pB
p_a_g_ta = p_ta_g_a * pA / p_ta

# With birth info
# P(species = A | test = A, twins, single) = P(test = A | A) * P(twins | A) *
#                                            P(single | A) * P(A) / P(test = A, twins, single)
# P(test = A, twins, single) = P(test = A | A) * P(twins | A) * P(single | A) * P(A) +
#
#
p_ta_twins_single = p_ta_g_a * ptwins_given_A * psingle_g_A * pA +
                    p_ta_g_b * ptwins_given_B * (1 - ptwins_given_B) * pB
p_a_g_ta_twins_single = p_ta_g_a * ptwins_given_A * psingle_g_A * pA / p_ta_twins_single


# Let's chart the inference
using DataFrames, Gadfly

evidence = ["prior",
            "prior + twins",
            "prior + twins, single",
            "prior + twins, single, test",
            "prior",
            "prior + twins",
            "prior + twins, single",
            "prior + twins, single, test"]
probA = [pA, pA_given_twins, pA_g_twins_single, p_a_g_ta_twins_single]
probB = 1.0 - probA
species = ["A", "A", "A", "A", "B", "B", "B", "B"]
probs = [pA, pA_given_twins, pA_g_twins_single, p_a_g_ta_twins_single,
         1-pA, 1-pA_given_twins, 1 - pA_g_twins_single, 1 - p_a_g_ta_twins_single]
df = DataFrame(species = species, evidence = evidence, prob = probs)
plot(df, x=:evidence, y=:prob, color=:species, Geom.point, Geom.line)
@
