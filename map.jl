using JuMP, Ipopt, Distributions, Gadfly

function map_quadapprox(data)
   # Goal: argmax θ f(X|θ)g(θ)
   #       where f is the likelihood and g the prior
   # where f(X|θ) is, say, pdf(Binomial(n, θ), k)
   # and g(θ) is the PDF of the prior at θ
   m = Model(solver=IpoptSolver(print_level=0))
   @variable(m, μ, start = 0.0)
   @NLconstraint(m, σ >= 0.0, start=1.0)
   @NLobjective(m, Max, )
   solve(m)
   return getvalue(θ), getobjectivevalue(m)
end

function binom_likelihood(θ, data)
   pdf(Binomial(data[:n], θ), data[:k])
end

function uniform_prior(θ, data)
   1.0 / data[:n]
end

θ, pθ = map_optim([0 1 1 1 0 1 0 1 0 1])
println("θ = ", θ)
println("MAP objective: ", pθ)
