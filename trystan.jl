using Mamba, Stan

# Define the stan model
const bernoullistanmodel = """
data {
  int<lower=0> N;
  int<lower=0,upper=1> y[N];
}
parameters {
  real<lower=0,upper=1> theta;
}
model {
  theta ~ beta(1,1);
    y ~ bernoulli(theta);
}
"""

# %%
ENV["CMDSTAN_HOME"] = "/usr/local/opt/cmdstan"

# %% Turn it into something useful
stanmodel = Stanmodel(name="bernoulli", model=bernoullistanmodel)

stanmodel |> display
# This is our observed data
const bernoullidata = Dict("N" => 10, "y" => [0, 1, 0, 1, 0, 0, 0, 0, 0, 1])

## %% STAN. IT. UP.
rc, sim1 = stan(stanmodel, [bernoullidata], pwd(), CmdStanDir=ENV["CMDSTAN_HOME"])
