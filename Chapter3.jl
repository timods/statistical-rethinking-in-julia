# Chapter 3: Sampling the Imaginary
# %% frequency-based
p_v = 0.001
p_m = 1 - p_v
p_t_g_v = 0.95
p_t_g_m = 0.01
p_t = p_t_g_v * p_v + p_t_g_m * p_m
p_v_g_t = p_t_g_v * p_v / p_t

# ...or...
v_p = 95
m_p = 999
p_v_p = v_p / (v_p + m_p)
@assert p_v_g_t == p_v_p

pr_pos_t = 0.95
pr_t = 0.01
pr_pos = pr_pos_t * pr_t + (1 - pr_pos_t) * (1 - pr_t)
pr_t_pos = pr_pos_t * pr_t / pr_pos

# %%
using Gadfly, Distributions, StatsBase

function grid_approx(dpdf, ps, priorfn::Function; samples=1000)
   p_grid = linspace(0, 1, samples)
   prior = priorfn(p_grid, ps)
   likelihood = dpdf(p_grid, ps) #[pdf(Binomial(ps[:k], p), ps[:w]) for p ∈ p_grid]
   unstd_posterior = likelihood .* prior
   posterior = unstd_posterior / sum(unstd_posterior)
   (p_grid, posterior)
end

function plot_approx(dpdf, params, priorfn; do_plot=false)
   p_grid, posterior = grid_approx(dpdf, params, priorfn)
   if do_plot
      plot(x=p_grid, y=posterior, Geom.line,
           Guide.xlabel("P(W)"),
           Guide.ylabel("posterior probability"))
   end
   return p_grid, posterior
end

uniform_prior = (grid, ps) -> ones(length(grid))
dbinom = ((grid, ps) -> [pdf(Binomial(ps[:n], p), ps[:k]) for p ∈ grid])

g_grid, g_post = grid_approx(dbinom, Dict(:n=>9, :k=>6), uniform_prior)
samples = sample(collect(g_grid), pweights(g_post), 10000; replace=true)
plot(layer(x=samples, Geom.density), Guide.title("Posterior density"),
     Guide.xlabel("Probability of water"), Scale.x_continuous(minvalue=0.0, maxvalue=1.0))

#' What is the posterior probability that the proportion of water is less than 0.5?
sum(g_post[g_grid .< 0.5 ])
sum(samples .< 0.5) / length(samples)

# How much posterior probability lies between 0.5 and 0.75?
sum((samples .> 0.5) .& (samples .< 0.75)) / length(samples)

# 80th percentile (lower 80%)
pct80 = quantile(samples, 0.8)
plot(layer(x=samples[samples .<= pct80], Geom.histogram),
     layer(x=samples[samples .> pct80], Geom.histogram, Theme(default_color="blue")))

# middle 80%
pct10 = quantile(samples, 0.1)
pct90 = quantile(samples, 0.9)
plot(layer(x=samples[samples .<= pct10], Geom.histogram, Theme(default_color="blue")),
     layer(x=samples[pct10 .<= samples .<= pct90], Geom.histogram, Theme(default_color="red")),
     layer(x=samples[samples .>= pct90], Geom.histogram, Theme(default_color="blue")))

p_grid, post = grid_approx(dbinom, Dict(:n=>3, :k=>3), uniform_prior)
sum(post .* abs(0.5 .- p_grid))
samples = sample(collect(p_grid), pweights(post), 10000; replace=true)
mean(samples)
linear_err = d -> sum(post .* abs(d .- p_grid))
loss = linear_err.(p_grid)
p_grid[findmin(loss)[2]]
median(samples)


# Why is it that the minimizer of our loss function is equivalent to the posterior
# median (within sample variation)?

# In order to decide upon a point estimate, we must choose a loss function. Absolute loss (above)
# leads to the median as the point estimate, and quadratic loss $(d - p)^2$ leads to the
# posterior mean as the point estimate:
# %%
function msqe(point, data, posterior)
  return sum(posterior .* abs((point .- data).^2))
end
quad_loss = [msqe(d, p_grid, post) for d in p_grid]
p_grid[findmin(quad_loss)[2]]
mean(samples)


#With a symmetric and normal-looking posterior the median and mean converge to the
#same point.

## Sampling to simulate prediction

# %%
# Probability of all possibilities for 2 tosses of the globe
using DataFrames
dummy_w = rand(Binomial(9, 0.7), Int(1e5))
dummy_post = [count( d -> d == v, dummy_w) for v in unique(dummy_w)]./1e5
dummy_df = DataFrame(x=unique(dummy_w), y=dummy_post)
plot(dummy_df, x=:x, y=:y, Geom.hair,
     Guide.xlabel("k"), Guide.ylabel("Pr(k)"),
     Guide.title("Probability of the true value of k"))
rand(Binomial(9, dummy_post), 10000)

# Simulating predictions from the total posterior
# This lets us check out how good closely the predictions from our posterior match
# the actual data (and let us check how sane & useful our model is!)
function sample_post(dist, posterior, n)
  # For each sampled value of k ∈ samples above we draw one random deviate from the distribution
  return collect(flatten([rand(dist(9, posterior[(i % length(posterior)) + 1]), 1) for i = 0:n]))
end

function measure_runs(data)
  runs = []
  prev = data[1]
  rl = 1
  for dp ∈ data[2:length(data)]
    if dp == prev
      rl += 1
    else
      # switch
      push!(runs, rl)
      rl = 1
    end
    prev = dp
  end
  push!(runs, rl)
  return runs
end

function plot_counts(data, normalize=true; title="Frequency of count", xlabel="count", ylabel="Frequency")
  counts = [count(d -> d == v, data) for v in unique(data)]
  if normalize
    counts = counts ./ length(data)
  end
  df = DataFrame(x=unique(data), y=counts)
  plot(df, x=:x, y=:y, Geom.hair,
       Guide.xlabel(xlabel), Guide.ylabel(ylabel),
       Guide.title(title))
end

using Base.Iterators
# Simulate predictions from the posterior
posterior_predictions = sample_post(post)
plot_counts(posterior_predictions; title="Posterior predictive distribution",
            ylabel="Pr(k)", xlabel="number of water samples")

# More model checking: count of run lengths

runs = measure_runs(posterior)
plot_counts(runs[runs .> 1], false; xlabel="longest run length")


## Practice
# %%
# setup
p_grid, posterior = grid_approx(dbinom, Dict(:n=>9, :k=>6), uniform_prior)
srand(100)
samples = sample(collect(p_grid), pweights(posterior), 10000; replace=true)

# 3E1: How much posterior probability lies below p = 0.2?
sum(posterior[p_grid .< 0.2 ])
# or
sum(samples .< 0.2) / length(samples)

# 3E2: How much posterior probability lies above p = 0.8?
sum(posterior[p_grid .> 0.8])
sum(samples .> 0.8) / length(samples)

# 3E3: How much lies on (0.2, 0.8)?
sum(0.2 .< samples .< 0.8) / length(samples)
# 3E4: 20% of the posterior probability lies below which value of p?

pct20 = quantile(samples, 0.2)
# 3E5: 20% of the posterior probability lies _above_ which value of p?
pct80 = quantile(samples, 0.8)

# 3E6: Which values of p contain the narrowest interval equal to 66% of the
#      posterior probability?
# this should be the 66% HDPI

# 3E7: Which values of p contain 66% of the posterior prob, assuming equal
#      posterior probability both above and below the interval?
# This will be the 66% credible interval
bar = (1.0 - .66)/2
lower = quantile(samples, bar)
upper = quantile(samples, 1.0 - bar)
plot(layer(x=samples[samples .<= lower], Geom.histogram, Theme(default_color="blue")),
     layer(x=samples[lower .<= samples .<= upper], Geom.histogram, Theme(default_color="orange")),
     layer(x=samples[samples .>= upper], Geom.histogram, Theme(default_color="blue")),
     Scale.x_continuous(minvalue=0.0, maxvalue=1.0))

# 3M1: Suppose the globe tossing data had turned out to be k=8, n=15. Construct
#      the posterior using the grid approximation and a uniform prior
p_grid, posterior = grid_approx(dbinom, Dict(:n=>15, :k=>8), uniform_prior)
median(posterior)
# 3M2: Draw 10k samples from the approximation above, and calculate the 90% HDPI
samples = sample(collect(p_grid), pweights(posterior), 10000; replace=true)

# TODO: figure out how to calculate a HDPI...

# 3M3: Construct a posterior predictive check for this model and the data. This
#      means simulate the distribution of samples, averaging over the posterior
#      uncertainty in p. What is the probability of observing 8 water in 15 tosses?
post_samples = sample_post(Binomial, samples, 10000)
plot(x=post_samples, Geom.histogram)
sum(post_samples .== 8) / length(post_samples)
# 3M4: Using the posterior predictive distribution for the (8/15) data, calculate
#      the probability of observing 6 water in 9 tosses

# 3M5: Start at 3M1 but use a prior that is 0 below p=0.5 and constant above.
#      Repeat each of the above problems and compare the inferences.
#      What differences does the better prior make?
